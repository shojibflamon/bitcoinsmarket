<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'name',
        'name',
        'infoemail',
        'supportemail',
        'url',
        'referral_comission',
        'bitcoin_buy_comission',
        'bitcoin_sell_comission',
        'litecoin_buy_comission',
        'litecoin_sell_comission',
        'dogecoin_buy_comission',
        'dogecoin_sell_comission',
        'bitcoin_withdrawal_comission',
        'litecoin_withdrawal_comission',
        'dogecoin_withdrawal_comission',
        'profits',
        'document_verification',
        'email_verification',
        'phone_verification',
        'recaptcha_verification',
        'recaptcha_publickey',
        'recaptcha_privatekey',
        'bitcoin_status',
        'litecoin_status',
        'dogecoin_status',
        'nexmo_api_key',
        'nexmo_api_secret',
    ];
}
