<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'secret_pin', 'email_verified', 'email_hash',
        'document_verified', 'document_1', 'document_2', 'mobile_verified', 'mobile_number', 'status',
        'hash', 'ip', 'time_signup', 'time_signin', 'time_activity', 'referral_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
