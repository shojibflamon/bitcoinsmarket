<?php

namespace App\Http\Controllers;


use App\SmsCode;
use App\Trade;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Nexmo\Laravel\Facade\Nexmo;
use App\BlockIoLicense;
use App\UsersTransaction;
use App\Helpers\BlockIo;


class AccountsController extends Controller
{
    public function setting()
    {
        $title = "Setting";
        $loginUser = Auth::user();
        $model = Trade::where('uid',$loginUser->id)
            ->orWhere('trader',$loginUser->id)
            ->get();

        return view('account.setting')
            ->with('title',$title)
            ->with('model',$model);

    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'old_password'		    => 'required|min:8|max:32',
            'password' 		        => 'required|min:8|max:32|confirmed',
        ]);

        $login_user = Auth::user();
        $db_pass = $login_user->password;
        $old_password = $request->get('old_password');
        $newPass = $request->get('password');

        //Hash::check('plain-text', $hashedPassword)
        if(Hash::check($old_password, $db_pass)) {
            $data = [
                'password' => bcrypt($newPass)
            ];
            $login_user->update($data);
            return back()
                ->with('alert_class', 'success')
                ->with('flash-message', 'Successfully Changed.');
        }else{
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', 'Current password dose not match.');
        }
    }

    public function verification()
    {
        $title = "Verification";

        return view('account.verification')
            ->with('title',$title);


    }

    public function doVerification(Request $request)
    {
        $title = "Verification";
        $verificationType = $request->get('verify');
        $loginUser = Auth::user();

        if($verificationType == 'email'){


            $email = getUserInfo($loginUser->id)->email;
            $hash = md5($email);
            $loginUser->hash = $hash;
            $loginUser->save();

            $msubject = '['.getSettingsData()->name.'] Email verification';
            $mreceiver = $email;
            $message = 'Hello, '.$email.'
        
        To activate your account and make exchanges need to confirm your email address. Click on the link below:
        '.getSettingsData()->url.'email-verify/'.$hash.'
        
        If you have some problems please feel free to contact with us on '.getSettingsData()->supportemail;
            $headers = 'From: '.getSettingsData()->infoemail.'' . "\r\n" .
                'Reply-To: '.getSettingsData()->infoemail.'' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
            $mail = mail($mreceiver, $msubject, $message, $headers);
            if($mail) {
                $emailStatus = true;
            } else {
                $emailStatus = false;
            }
            return view('account.verification')
                ->with('emailStatus',$emailStatus)
                ->with('title',$title);
        }elseif($verificationType == 'doc'){
            $file_name = $this->file_upload('document_1',$request,'documents');
            $loginUser->document_1 = $file_name;
            $file_name = $this->file_upload('document_2',$request,'documents');
            $loginUser->document_2 = $file_name;
            $loginUser->save();

            if($loginUser) {
                $uploadStatus = true;
            } else {
                $uploadStatus = false;
            }

            return view('account.verification')
                ->with('uploadStatus',$uploadStatus)
                ->with('title',$title);

        }elseif ($verificationType == 'add_number'){
            $loginUser->mobile_number = $request->get('mobile_number');
            $loginUser->save();
             if($loginUser) {
                $addNumber = true;
            } else {
                $addNumber = false;
            }
             return view('account.verification')
                ->with('addNumber',$addNumber)
                ->with('title',$title);

        }elseif ($verificationType == 'send_sms_code'){
            if($request->has('btc_send_sms_code')){

                // Step 2: Use sendText( $to, $from, $message ) method to send a message.
                $rand = rand(00000,99999);
                $number = getUserInfo($loginUser->id)->mobile_number;
                $smsCode = SmsCode::create([
                    'uid' => $loginUser->id,
                    'sms_code' => $rand,
                    'verified' => '0',
                ]);
                $message = 'Your code for '.getSettingsData()->name.' is: '.$rand.' ';

                $error = false;
                try{
                    $info = Nexmo::message()->send([
                        'to'   => '+'.$number,
                        'from' => getSettingsData()->name,
                        'text' => $message
                    ]);
                }catch (\Exception $e){
                    $errorMsg = $e->getMessage();
                    $error = true;
                }

                if($smsCode) {
                    $codeSendToSms = true;
                } else {
                    $codeSendToSms = false;
                }
                if($error){
                    return back()
                        ->with('alert_class','danger')
                        ->with('flash-message',$errorMsg)
                        ->with('title',$title);
                }else{
                     return view('account.verification')
                        ->with('codeSendToSms',$codeSendToSms)
                        ->with('number',$number)
                        ->with('title',$title);
                }

            }elseif ($request->has('btc_verify_sms_code')){
                $sms_code = $request->get('sms_code');

                $sms_code_model = SmsCode::where('uid',$loginUser->id)->where('sms_code',$sms_code)->where('verified',0)->first();

                if(empty($sms_code_model)) {
                    echo __('crypto.error_42');
                }elseif(count( $sms_code_model) == 0) {
                    echo __('crypto.error_43');
                }else {
                    $sms_code_model->verified = 1;
                    $sms_code_model->save();
                    $loginUser->mobile_verified = 1;
                    $loginUser->save();

                    if($loginUser) {
                        $verifyCode = true;
                    } else {
                        $verifyCode = false;
                    }
                    return view('account.verification')
                        ->with('verifyCode',$verifyCode)
                        ->with('title',$title);
                }
            }

        }

    }

    private function file_upload($field_name,$request,$path)
	{
	    $this->validate($request, [
            $field_name		    => 'required|mimes:jpeg,png,jpg,pdf',
        ]);

		if($request->hasFile($field_name)){
			$file = $request->file($field_name);
			$file_name = str_random(8).'_'.$file->getClientOriginalName();

			$destinationPath = $path.'/';
			$file->move($destinationPath,$file_name);
			return $file_name;
		}
	}

	public function wallet()
    {
        $title = "My Wallet";
        $loginUser = Auth::user();
        $model = UsersTransaction::where('uid',$loginUser->id)->get();
        $trades = Trade::where('uid',$loginUser->id)
            ->where('status','<',3)
            ->orWhere(function ($query) use ($loginUser){
                $query->where('trader', $loginUser->id);
                $query->where('status','<', 3);
            })
            ->get();

        return view('account.wallet')
            ->with('title',$title)
            ->with('model',$model)
            ->with('trades',$trades);
    }

    public function walletSend(Request $request)
    {
        $loginUser = Auth::user();

        $from_wallet = $request->get('from_wallet');

        if($from_wallet == "Bitcoin") {
            $withdrawal_comission = getSettingsData()->bitcoin_withdrawal_comission;
            $wfee = 0.0008;
            $wfee2 = 0.0004;
            $mamm = 0.0001;
            $ext = 'BTC';
        } elseif($from_wallet == "Litecoin") {
            $withdrawal_comission = getSettingsData()->litecoin_withdrawal_comission;
            $wfee = 0.005;
            $wfee2 = 0.003;
            $mamm = 0.001;
            $ext = 'LTC';
        } elseif($from_wallet == "Dogecoin") {
            $withdrawal_comission = getSettingsData()->dogecoin_withdrawal_comission;
            $wfee = 4;
            $wfee2 = 2;
            $mamm = 1;
            $ext = 'DOGE';
        } else { }

        $amount = $request->get('amount');
        $orig_amount = $request->get('amount');
        $recipient = $request->get('recipient');

        $min_amount = $withdrawal_comission+$wfee;

       $checkreports = Trade::where('trader',$loginUser->id)
           ->where('status',0)
           ->orWhere('uid',$loginUser->id)
           ->Where('status',0)->get();


        if(!is_numeric($amount)) {
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_35'));
        }elseif(count($checkreports) > 0) {
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_46'));
        }elseif($amount < $min_amount) {
            $lang_error_36 = str_ireplace("%min_amount%",$min_amount.' '.$ext,__('crypto.error_36'));
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message',$lang_error_36);
        }elseif(empty($recipient)) {
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_37'));
        }else {
            $amount = $amount - $withdrawal_comission - $wfee2;

            $ubalance = walletinfo($loginUser->id,"available_balance",$from_wallet);

            $min_amount1 = $min_amount+$mamm;
            if($ubalance < $min_amount) {
                $lang_error_38 = str_ireplace("%min_amount%",$min_amount1.' '.$ext,__('crypto.error_38'));
                return back()
                    ->with('alert_class', 'danger')
                    ->with('flash-message', $lang_error_38);
            }
            else {
                $uaddress = walletinfo($loginUser->id,"address",$from_wallet);
                $lid = walletinfo($loginUser->id,"lid",$from_wallet);

                $license = BlockIoLicense::find($lid);

                $apiKey = $license->license;
                $pin = $license->secret_pin;
                $version = 2; // the API version
                $block_io = new BlockIo($apiKey, $pin, $version);

                $error = false;
                try{
                    $withdrawal = $block_io->withdraw_from_addresses(array('amounts' => $amount, 'from_addresses' => $uaddress, 'to_addresses' => $recipient));
                    $withdrawal = $block_io->withdraw_from_addresses(array('amounts' => $withdrawal_comission, 'from_addresses' => $uaddress, 'to_addresses' => $license->address));
                }catch (\Exception $e){
                    $errorMsg = $e->getMessage();
                    $error = true;
                }

                $lang_success_14 = str_ireplace("%orig_amount%",$orig_amount.' '.$ext,__('crypto.success_14'));
                $lang_success_14 = str_ireplace("%recipient%",$recipient,$lang_success_14);

                if($error){
                    return back()
                        ->with('alert_class', 'danger')
                        ->with('flash-message', $errorMsg);
                }
                return back()
                    ->with('alert_class', 'success')
                    ->with('flash-message', $lang_success_14);
            }
        }
    }

    public function transaction()
    {
        $title = "Transaction";
        $loginUser = Auth::user();
        $model = UsersTransaction::where('uid',$loginUser->id)->get();
        return view('account.transaction')
            ->with('title',$title)
            ->with('model',$model);

    }
}
