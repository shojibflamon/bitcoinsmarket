<?php

namespace App\Http\Controllers;

use App\TradeMessage;
use Illuminate\Http\Request;
use Auth;

class ChatsController extends Controller
{
    public function addTradeMsg(Request $request)
    {
        $loginUser = Auth::user();
        $message = $request->get('message');
        $tradeId = $request->get('trade_id');
        $time = time();
        if(empty($message) or empty($tradeId) or empty($loginUser)) {
            $data['status'] = 'error';
            $data['msg'] = 'ERROR';
        } else {
            TradeMessage::create([
                'uid' => $loginUser->id,
                'trade_id' => $tradeId,
                'readed' => "0",
                'message' => $message,
                'time' => $time,
            ]);

            $data['status'] = 'success';

            $data['msg'] = '<div style="font-size:14px;">
                <b><a href="">'.getUserInfo($loginUser->id)->username.'</a></b>: '.$message.'<br/>
                <span class="text text-muted" style="font-size:11px;">'.timeago($time).'</span>
            </div>
            <hr/>';

        }
        echo json_encode($data);

    }

    public function uploadFromChat(Request $request)
    {
        $loginUser = Auth::user();
        $time = time();
        $trade_id = $request->get('trade_id');
        $message = $request->get('message');
        $path = 'uploads/attachments/'.$loginUser->id.'_'.$time;
        $file_name = $this->file_upload('uploadFile',$request,$path);

        TradeMessage::create([
            'uid' => $loginUser->id,
            'trade_id' => $trade_id,
            'readed' => "0",
            'message' => $path.'/'.$file_name,
            'attachment' => '1',
            'time' => $time,
        ]);
    }

    public function checkNewFileMessage(Request $request)
    {
        $loginUser = Auth::user();
        $uid = $request->get('uid');
        $trade_id = $request->get('trade_id');

        $time = time()-60;
        $tradeMsgModel = TradeMessage::where('uid',$uid)
            ->where('trade_id',$trade_id)
            ->where('attachment',1)
            ->where('time','>',$time)
            ->orderBy('id','DESC')
            ->first();

        if(count($tradeMsgModel)>0){
            $filename = basename($tradeMsgModel->message);
            $data['status'] = 'success';
            $data['msg'] = '<div style="font-size:14px;">
                        <b><a href="">'.getUserInfo($loginUser->id)->username.'</a></b> attach file<br/>
                        <i class="fa fa-file-o"></i> <a href="'.url('/'.$tradeMsgModel->message).'" target="_blank">'.$filename.'</a><br/>
                        <span class="text text-muted" style="font-size:11px;">'.timeago($tradeMsgModel->time).'</span>
                    </div>
                    <hr/>';
        }else{
            $data['status'] = 'error';
            $data['msg'] = 'No data';

        }

        echo json_encode($data);
    }

    public function checkNewMessage(Request $request)
    {
        $loginUser = Auth::user();
        $uid = $request->get('uid');
        $trade_id = $request->get('trade_id');

        $tradeMsgModel = TradeMessage::where('uid','!=',$uid)
            ->where('trade_id',$trade_id)
            ->where('readed',0)
            ->orderBy('id','DESC')
            ->get();

        if(count($tradeMsgModel)>0){
            foreach ($tradeMsgModel as $single){
                if($single->uid == $loginUser->id) {
                    $data['status'] = 'error';
                    $data['msg'] = 'User is the some';
                } else {
                    $data['status'] = 'success';
                    if($single->attachment) {
                        $filename = basename($single->message);
                        $data['msg'] = '<div style="font-size:14px;">
                        <b><a href="'.route('userProfile',getUserInfo($single->uid)->username).'">'.getUserInfo($loginUser->id)->username.'</a></b> attach file<br/>
                        <i class="fa fa-file-o"></i> <a href="'.url('/'.$single->message).'" target="_blank">'.$filename.'</a><br/>
                        <span class="text text-muted" style="font-size:11px;">'.timeago($single->time).'</span>
                    </div>
                    <hr/>';
                    } else {
                    $data['msg'] = '<div style="font-size:14px;">
                        <b><a href="'.route('userProfile',getUserInfo($single->uid)->username).'">'.getUserInfo($loginUser->id)->username.'</a></b>: '.$single->message.'<br/>
                        <span class="text text-muted" style="font-size:11px;">'.timeago($single->time).'</span>
                    </div>
                    <hr/>';
                    }
                    TradeMessage::find($single->id)->update(['readed'=>1]);

                }
            }
        }else{
            $data['status'] = 'error';
            $data['msg'] = 'No data';

        }

        echo json_encode($data);
    }

    private function file_upload($field_name,$request,$path)
	{
	    $this->validate($request, [
            $field_name		    => 'required|mimes:jpeg,png,jpg,pdf',
        ]);

		if($request->hasFile($field_name)){
			$file = $request->file($field_name);
			$file_name = str_random(8).'_'.$file->getClientOriginalName();

			$destinationPath = $path.'/';
			$file->move($destinationPath,$file_name);
			return $file_name;
		}
	}
}
