<?php

namespace App\Http\Controllers;

use App\BlockIoLicense;
use App\PostAdd;
use App\Trade;
use App\TradeReport;
use App\UsersNotification;
use App\UsersRating;
use Illuminate\Http\Request;
use Auth;
use App\Helpers\BlockIo;


class TradesController extends Controller
{
    public function trade()
    {
        $title = "Trade";
        $loginUser = Auth::user();
        $model = Trade::where('uid',$loginUser->id)
            ->orWhere('trader',$loginUser->id)
            ->orderBy('id','DESC')
            ->get();

        return view('account.trade')
            ->with('title',$title)
            ->with('model',$model);

    }

    public function tradeCreate(Request $request)
    {
        $loginUser = Auth::user();

        $ad_id = $request->get('ad_id');
        $crypto_price = $request->get('crypto_price');
        $crypto_amount = $request->get('crypto_amount');

        $postModal = PostAdd::find($ad_id);

        $typeArr = explode('_',$postModal->type);
        if($typeArr[0] == 'buy'){
            $field_name = $typeArr[1].'_sell_comission';
             $type = 'sell_'.$typeArr[1];
        }elseif($typeArr[0] == 'sell'){
            $field_name = $typeArr[1].'_buy_comission';
             $type = 'buy_'.$typeArr[1];
        }

        $comission_rate = getSettingsData()->$field_name;

        $userbalance = get_user_balance($loginUser->id,$postModal->network);
        $traderbalance = get_user_balance($postModal->uid,$postModal->network);

        $maxb = $traderbalance - 0.0008 - $comission_rate - 0.0001;
        $amountwithfees = $crypto_amount + 0.0008 + $comission_rate + 0.0001;
        $userbalance2 = $userbalance - 0.0008 - $comission_rate - 0.0001;

        $maxa = $maxb * $crypto_price;
        $maxa = number_format($maxa,2);
        $hash = randomHash(10);
        $payment_hash = strtoupper($hash);
        if($maxa < 0) { $maxa = 0; }


        if($userbalance2 < 0) { $userbalance2 = 0; }
        $amount = $request->get('amount');

        if(empty($amount) or empty($crypto_amount)) {
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_1'));

        }elseif($postModal->min_amount > $amount) {
            $lang_error_3 = str_ireplace("%min_amount%",$postModal->min_amount,__('crypto.error_3'));
            $lang_error_3 = str_ireplace("%currency%",$postModal->currency,$lang_error_3);
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', $lang_error_3);
        }elseif($postModal->max_amount < $amount) {
            $lang_error_4 = str_ireplace("%max_amount%",$postModal->max_amount,__('crypto.error_4'));
            $lang_error_4 = str_ireplace("%currency%",$postModal->currency,$lang_error_4);
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', $lang_error_4);
        }
		elseif($traderbalance < $amountwithfees) {
            $lang_error_6 = str_ireplace("%amount%",$maxa,__('crypto.error_6'));
            $lang_error_6 = str_ireplace("%currency%",$postModal->currency,$lang_error_6);
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', $lang_error_6);
            }
		elseif($postModal->require_document == "1" &&  getUserInfo($loginUser->id)->document_verified != "1") {
            $msg = __('crypto.ad_require_doc_verify') .' '.__('crypto.please_go_to_tab') .' '.__('crypto.menu_verification');
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message',$msg );
            }
		elseif($postModal->require_email == "1" && getUserInfo($loginUser->id)->email_verified != "1") {
            $msg = __('crypto.ad_require_email_verify') .' '.__('crypto.please_go_to_tab') .' '.__('crypto.menu_verification');
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message',$msg );
           }
		elseif($postModal->require_mobile == "1" && getUserInfo($loginUser->id)->mobile_verified != "1") {
            $msg = __('crypto.ad_require_mobile_verify') .' '.__('crypto.please_go_to_tab') .' '.__('crypto.menu_verification');
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message',$msg );
            }

		$timeout = $postModal->process_time * 60;
        $timeout = time() + $timeout;
        $time = time();

        $tradreData = [
            'uid' => $loginUser->id,
            'type' => $type,
            'network' => $postModal->network,
            'ad_id' => $postModal->id,
            'trader' => $postModal->uid,
            'payment_hash' => $payment_hash,
            'crypto_price' => $crypto_price,
            'crypto_amount' => $crypto_amount,
            'amount' => $amount,
            'payment_instructions' => $postModal->payment_instructions,
            'status' => 1,
            'created' => $time,
            'timeout' => $timeout,
        ];

        $tradreModel = Trade::create($tradreData);

        $userNotificationData = [
            'uid' => $postModal->uid,
            'notified' => '0',
            'trade_id' => $tradreModel->id,
            'time' => $time,
        ];
        $userNotificationModel = UsersNotification::create($userNotificationData);

        return redirect()->route('account.singleTrade',$tradreModel->id);

    }

    public function singleTrade($id)
    {
        $tradeModel = Trade::find($id);
        $title = 'Trade';
        return view('trade')
            ->with('title',$title)
            ->with('model',$tradeModel);

    }

    public function cancelTrade($trade_id)
    {
         $tradeModel = Trade::find($trade_id);
         $tradeModel->status = 5;
         $tradeModel->save();

         return view('account.cancel-trade')
            ->with('title','Canclation Trade')
            ->with('msg','You cancel this trade.');

    }

    public function tradeReportForm($trade_id)
    {
         $tradeModel = Trade::find($trade_id);

         return view('tradings.trade-report')
            ->with('title','Trade Report')
            ->with('model',$tradeModel);

    }

    public function tradeReportSave(Request $request,$trade_id)
    {
        $loginUser = Auth::user();
        $content = $request->get('content');
        $time = time();
        if(empty($content)) {
            return back()
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_26'));
        }else {
            $tradeReport = TradeReport::create([
                'uid' => $loginUser->id,
                'trade_id' => $trade_id,
                'content' => $content,
                'status' => "0",
                'time' => $time,
            ]);
        }
        return back()
            ->with('alert_class', 'success')
            ->with('flash-message', __('crypto.success_5').$tradeReport->id);

    }

    public function processTrade(Request $request)
    {
        $loginUser = Auth::user();
        $trade_id = $request->get('trade_id');
        $minutes = $request->get('minutes');
        $tradeModel = Trade::find($trade_id);

        if($request->has('btc_payment_was_made')){

            if($minutes !== "0") {
                 return back()
                    ->with('form','warning' )
                    ->with('form_name','btc_payment_was_made' );
            }

        }elseif($request->has('btc_cancel_trade')){
            if($tradeModel->status >1 or $tradeModel->status == "0") {
                $msg = __('crypto.error_47');
                return back()
                    ->with('alert_class', 'danger')
                    ->with('flash-message',$msg );
            } else {
                $tradeModel->status = 4;
                $tradeModel->save();
                $msg = __('crypto.info_2');
                return back()
                    ->with('alert_class', 'info')
                    ->with('flash-message',$msg );
            }
        }elseif($request->has('btc_pwm_confirmed')){

            if($tradeModel->status != "2" or $tradeModel->status < "2") {
                $tradeModel->status = 2;
                $tradeModel->save();
                $msg = __('crypto.success_8');
                return back()
                    ->with('alert_class', 'success')
                    ->with('flash-message',$msg );
               }
        }elseif($request->has('btc_release_bitcoins')){
            if($tradeModel->released_bitcoins == "0") {
                if($minutes != "0") {
                     return back()
                        ->with('form','warning' )
                        ->with('form_name','btc_release_bitcoins' );
                }else{
                    $msg = __('crypto.trade_info_4');
                    return back()
                        ->with('alert_class','danger' )
                        ->with('flash-message', $msg);
                }
            }
        }elseif($request->has('btc_relaese_bitcoins_confirmed')){
            if($minutes != "0") {
                if(!$tradeModel->released_bitcoins) {

                    $uaddress = walletinfo($tradeModel->uid,"address",$tradeModel->network);

                    $taddress = walletinfo($tradeModel->trader,"address",$tradeModel->network);

                    $lid = walletinfo($loginUser->id,"lid",$tradeModel->network);

                    $license = BlockIoLicense::find($lid);

                    $apiKey = $license->license;
                    $pin = $license->secret_pin;
                    $version = 2; // the API version
                    $block_io = new BlockIo($apiKey, $pin, $version);
                    $error = false;
                    try{
                        $withdrawal = $block_io->withdraw_from_addresses(array('amounts' => $tradeModel->crypto_amount, 'from_addresses' => $taddress, 'to_addresses' => $uaddress));
                        $withdrawal = $block_io->withdraw_from_addresses(array('amounts' => $tradeModel->bitcoin_buy_comission, 'from_addresses' => $taddress, 'to_addresses' => $license->address));

                        $tradeModel->status = 7;
                        $tradeModel->released_bitcoins = 1;
                        $tradeModel->save();
                    }catch (\Exception $e){
                        $errorMsg = $e->getMessage();
                        $error = true;
                    }

                    if($error){
                        $msg = 'Cannot withdraw funds without Network Fee of 0.00000000 BTCTEST. Maximum withdrawable balance is 0.00000000 BTCTEST';
                         return back()
                            ->with('alert_class','danger' )
                            ->with('flash-message',$msg );

                    }else{
                        $msg = __('crypto.success_7');
                        return back()
                            ->with('alert_class','success' )
                            ->with('flash-message',$msg );
                    }
                }
            }
        }

    }

    public function leaveFeedbackFrom(Request $request, $id)
    {
        $loginUser = Auth::user();

        $trades = Trade::where('id',$id)
            ->where('uid',$loginUser->id)
            ->orWhere(function($query) use ($loginUser,$id){
                $query->where('id',$id);
                $query->where('trader',$loginUser->id);
            })
            ->first();
        if($trades->status != 7){
            return redirect()->route('account.trade');
        }

        $check_feedback = UsersRating::where('trade_id',$trades->id)
            ->where('author',$loginUser->id)
            ->first();

        if(count($check_feedback)>0){
            return redirect()->route('account.trade');
        }

       $network = tradeinfo($trades->trader,"network");

        if($network == "Bitcoin") {
            $ext = 'BTC';
        } elseif($network == "Litecoin") {
            $ext = 'LTC';
        } elseif($network == "Dogecoin") {
            $ext = 'DOGE';
        } else { }

        $title = 'Leave Feedback';
        return view('tradings.trade-feedback')
            ->with('model',$trades)
            ->with('ext',$ext)
            ->with('title',$title);
    }

    public function leaveFeedbackSave(Request $request,$id)
    {
        $loginUser = Auth::user();

        $trades = Trade::where('id',$id)
            ->where('uid',$loginUser->id)
            ->orWhere(function($query) use ($loginUser,$id){
                $query->where('id',$id);
                $query->where('trader',$loginUser->id);
            })
            ->first();

        $type = $request->get('type');
        $content = $request->get('content');
        $trade_id = $request->get('trade_id');

        if($trades->uid !== $loginUser->id) {
            $uid = $trades->uid;
        } elseif($trades->trader !== $loginUser->id) {
            $uid = $trades->trader;
        }
        $time = time();
        $author = $loginUser->id;
        $title = 'Leave Feedback';
        if(empty($type)) {
            return back()
                ->with('title',$title)
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_27'));
        }elseif(empty($content)) {
            return back()
                ->with('title',$title)
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_28'));
        }else {
            UsersRating::create([
                'uid' => $uid,
                'type' => $type,
                'trade_id' => $trades->id,
                'comment' => $content,
                'author' => $author,
                'time' => $time,
            ]);

             return back()
                ->with('title',$title)
                ->with('alert_class', 'success')
                ->with('flash-message', __('crypto.success_6'));

        }

    }
}
