<?php

namespace App\Http\Controllers;

use App\User;

class CronsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cronJob()
    {
        $users = User::all();
        foreach ($users as $single){
            btc_update_balance($single->id);
            ltc_update_balance($single->id);
            doge_update_balance($single->id);

            btc_update_transactions($single->id);
            ltc_update_transactions($single->id);
            doge_update_transactions($single->id);

            btc_delete_fee_transactions($single->id);
            ltc_delete_fee_transactions($single->id);
            doge_delete_fee_transactions($single->id);
        }

        echo btc_get_bitcoin_prices();
        echo ltc_get_litecoin_prices();
        echo doge_get_dogecoin_prices();

        btc_check_expired_trades();
    }
}
