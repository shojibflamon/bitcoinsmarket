<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRating extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'type',
        'trade_id',
        'comment',
        'author',
        'time',
    ];

}
