<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
        'answer',
        'network',
        'payment_method',
        'currency',
        'payment_instructions',
        'price',
        'min_amount',
        'max_amount',
        'process_time',
        'terms',
        'require_document',
        'require_email',
        'require_mobile',
    ];

}
