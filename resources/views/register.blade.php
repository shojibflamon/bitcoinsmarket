@extends('layouts.master')
@section('content')

    <div class="container-fluid login_register header_area deximJobs_tabs">
    	<div class="row">
            <div class="container main-container">
                <div class="col-lg-offset-1 col-lg-11 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-pills ">
                        <li class="active"><a href="{!! route('register') !!}">@lang('crypto.register')</a></li>
                        <li><a href="{!! route('login') !!}">@lang('crypto.login')</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="register-account" class="tab-pane fade in active white-text">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 zero-padding-left">

                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                       <i class="fa fa-check"></i> {{ session()->get('message') }}
                                    </div>
                                @else
                                    @include('flash-message')
                                @endif
                                @include('register-form')
                            </div>

                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12  pull-right sidebar">
                                <div class="widget">
                                    <h3>@lang('crypto.why_to_have_account_in') name </h3>
                                    <ul>
                                        <li><p><i class="fa fa-clock-o"></i> @lang('crypto.why_info_1')</p></li>
                                        <li><p><i class="fa fa-child"></i> @lang('crypto.why_info_2')</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
       </div>
    </div>

@endsection