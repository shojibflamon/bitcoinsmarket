@if(session('form_name') == 'btc_payment_was_made')
     <div class="alert alert-{{ session('form') }}" style="font-size:18px;">
         <form action="{{route('account.processTrade')}}" method="POST">
             <input type="hidden" name="trade_id" value="{{$model->id}}">
             {{csrf_field()}}
            <p>
                @lang('crypto.are_you_sure_made_payment')
                <a href="{{route('userProfile',$loginUser->username)}}">{{$loginUser->username}}</a>
                @lang('crypto.with_amount') {{$model->amount}}  {{adinfo($model->ad_id,"currency")}}</p>
            <small> @lang('crypto.this_action_can_be_undo')</small>
            <br/><br/>
            <button type="submit" class="btn btn-success" name="btc_pwm_confirmed"><i class="fa fa-check"></i>  @lang('crypto.btn_yes_made_payment')</button>
            <a href="" class="btn btn-danger"><i class="fa fa-times"></i>  @lang('crypto.btn_no')</a>
        </form>
    </div>
@endif

@if(session('form_name') == 'btc_release_bitcoins')
     <div class="alert alert-{{ session('form') }}" style="font-size:18px;">
         <form action="{{route('account.processTrade')}}" method="POST">
             <input type="hidden" name="trade_id" value="{{$model->id}}">
             {{csrf_field()}}
            <p>
                @lang('crypto.are_you_sure_release_bitcoins_2')
                <a href="{{route('userProfile',$loginUser->username)}}">{{$loginUser->username}}</a>?
                </p>
            <small> @lang('crypto.this_action_can_be_undo')</small>
            <br/><br/>
            <button type="submit" class="btn btn-success" name="btc_relaese_bitcoins_confirmed"><i class="fa fa-check"></i>  @lang('crypto.btn_yes_release_bitcoins')</button>
            <a href="" class="btn btn-danger"><i class="fa fa-times"></i>  @lang('crypto.btn_no')</a>
        </form>
    </div>
@endif

