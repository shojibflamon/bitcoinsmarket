<div class="panel panel-default">
    <div class="panel-body">
        <p style="font-size:16px;">
            @php
                $lang_trade_info_9 = str_ireplace("%amount%",$model->amount,__('crypto.trade_info_9'));
                $lang_trade_info_9 = str_ireplace("%currency%",adinfo($model->ad_id,"currency"),$lang_trade_info_9);
                $lang_trade_info_9 = str_ireplace("%payment_method%",adinfo($model->ad_id,"payment_method"),$lang_trade_info_9);
                echo $lang_trade_info_9;
            @endphp
        </p>
        <div class="alert alert-info">
            <b style="font-size:14px;">@lang('crypto.payment_instructions')</b><br/>
            {{nl2br($model->payment_instructions)}}<br/>

            @php
                $lang_trade_info_10 = str_ireplace("%payment_hash%",$model->payment_hash,__('crypto.trade_info_10'));
                echo $lang_trade_info_10;
            @endphp
        </div>
        <p style="font-size:14px;" class="text text-danger">@lang('crypto.trade_info_11')</p>
        <p>@lang('crypto.trade_info_3')</p>
        <br>

        <form action="{{route('account.processTrade')}}" method="POST">
            <input type="hidden" name="trade_id" value="{{$model->id}}">
            <input type="hidden" name="minutes" value="{{$minutes}}">
            {{csrf_field()}}
            @if($model->status < 3)
                @if($model->released_bitcoins == "0")
                    @if($model->status < 2)
                        @if($minutes !== "0")
                            <button type="submit" class="btn btn-success" name="btc_payment_was_made"><i class="fa fa-check"></i> @lang('crypto.btn_payment_was_made')</button>
                            <button type="submit" class="btn btn-danger" name="btc_cancel_trade" id="btc_cancel_trade"><i class="fa fa-times"></i> @lang('crypto.btn_cancel_trade')</button>
                        @endif
                    @endif
                @endif
            @endif
            <a href="{{route('account.tradeReportForm',$model->id)}}" class="btn btn-warning"><i class="fa fa-flag"></i> @lang('crypto.btn_report_trade')</a>

            @if($model->status == "7")
                @if(!checkFeedback($model->id))
                    <a href="{{route('leaveFeedbackFrom',$model->id)}}" class="btn btn-info"><i class="fa fa-comment"></i> @lang('crypto.btn_leave_feedback')</a>
                @endif
            @endif
        </form>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">

        @if($minutes == 0)
            @lang('crypto.trade_info_4')
        @else
            @php
                $lang_trade_info_5 = str_ireplace("%minutes%",$minutes,__('crypto.trade_info_5'));
                echo $lang_trade_info_5;
            @endphp

        @endif

    </div>
</div>