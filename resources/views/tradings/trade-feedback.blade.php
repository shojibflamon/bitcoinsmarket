@extends('layouts.master')
@section('content')

<div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="white-heading">@lang('crypto.leave_feedback')</h3>
					</div>
                </div>
            </div>
        </div>
  	 <!--header section -->

	<!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
							<div class="container">
								<div class="col-md-12" style="margin-top:10px;margin-bottom:30px;"	>
									<div class="row">
										<div class="col-md-12">
											<span style="font-size:35px;font-weight:bold;">@lang('crypto.ttrade') #{{$model->id}} <small>@lang('crypto.from_advertisement') <a href="" >#{{$model->ad_id}}</a>, @lang('crypto.bitcoin_price') {{$model->crypto_price}} {{ adinfo($model->ad_id,'currency')}}/BTC</span><br/>
											<span style="font-size:25px;">@lang('crypto.trade_amount'): <b> {{$model->amount}} {{adinfo($model->ad_id,'currency')}} </b> ({{$model->crypto_amount}} /{{$ext}})</span>

										</div>
									</div>
								</div>
							</div>
							<br>

							@include('flash-message')

							<form action="{{route('leaveFeedbackSave',$model->id)}}" method="POST">
								{{csrf_field()}}
								<div class="form-group">
									<label>@lang('crypto.choose_feedback_type')</label>
									<div class="radio">
									  <label>
										<input type="radio" name="type" value="1">
										<span class="text text-success"><i class="fa fa-smile-o"></i> @lang('crypto.positive')</span>
									  </label>
									</div>
									<div class="radio">
									  <label>
										<input type="radio" name="type" value="2">
										<span class="text text-warning"><i class="fa fa-meh-o"></i> @lang('crypto.neutral')</span>
									  </label>
									</div>
									<div class="radio">
									  <label>
										<input type="radio" name="type" value="3">
										<span class="text text-danger"><i class="fa fa-frown-o"></i> @lang('crypto.negative')</span>
									  </label>
									</div>
								</div>
								<div class="form-group">
									<label>Your feedback</label>
									@php
										if($model->uid !== $loginUser->id){
											$feedbackPlaceholder = __('crypto.leave_feedback_for').' '.getUserInfo($model->uid)->username;
										}elseif($model->trader !== $loginUser->id) {
											$feedbackPlaceholder = __('crypto.leave_feedback_for').' '.getUserInfo($model->trader)->username;
										}
									@endphp
									<textarea class="form-control" name="content" rows="10" placeholder="{{$feedbackPlaceholder}} .."></textarea>
								</div>
								<button type="submit" class="btn btn-primary" name="btc_feedback"><i class="fa fa-check"></i> @lang('crypto.btn_submit')</button>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>

@endsection