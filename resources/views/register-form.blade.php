<form method="POST" action="{{ route('register') }}" class="contact_us">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="username">@lang('crypto.username')</label>
        <input id="username" type="text" name="username" value="{{ old('username') }}" required autofocus>
        @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">@lang('crypto.email_address')</label>
        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password">@lang('crypto.password')</label>
        <input id="password" type="password" name="password" required>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="password-confirm">@lang('crypto.confirm_password')</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>

    <div class="form-group{{ $errors->has('secret_pin') ? ' has-error' : '' }}">
        <label for="secret_pin">@lang('crypto.secret_pin')</label>
        <input id="secret_pin" type="password" name="secret_pin" required>
        @if ($errors->has('secret_pin'))
            <span class="help-block">
                <strong>{{ $errors->first('secret_pin') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group submit">
        <label>Submit</label>
        <input type="submit" value="Register" class="register">
        <a href="{{route('showResetForm')}}" class="lost_password">@lang('crypto.forgot_password') </a>
    </div>

</form>