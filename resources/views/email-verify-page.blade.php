@extends('layouts.master')
@section('content')
    <div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
                		<h3 class="white-heading">@lang('crypto.email_verification')</h3>
                    </div>
                </div>
            </div>
        </div>
  	 <!--header section -->
	 <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
							@if($is_verify)
								<div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.success_2')</div>
							@else
								<div class="alert alert-danger"><i class="fa fa-crosshairs"></i> Not Verified.</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection