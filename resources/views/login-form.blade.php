<form method="POST" action="{{ route('login') }}" class="contact_us">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">@lang('crypto.email_address')</label>
        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password">@lang('crypto.password')</label>
        <input id="password" type="password" name="password" required>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group submit">
        <label>Submit</label>
        <div class="cbox">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
            <span>@lang('crypto.keep_me_login')</span>
            </label>
        </div>
    </div>

    <div class="form-group submit">
        <label>Submit</label>
            <input type="submit" name="btc_login" value="Login" class="signin" id="signin">
            <a href="{{route('showResetForm')}}" class="lost_password">@lang('crypto.forgot_password')</a>
    </div>
</form>