@extends('layouts.master')
@section('content')
    <!--header section -->
	<div class="container-fluid page-title">
		<div class="row blue-banner">
			<div class="container main-container">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<span style="color:#fff;font-size:35px;font-weight:bold;">
						@lang('crypto.ttrade') #{{$model->id}}
						<small>@lang('crypto.from_advertisement')
							<a href="addLink" style="color:#fff;">#{{$model->ad_id}}</a>, @lang('crypto.bitcoin_price') {{$model->crypto_price}} {{adinfo($model->ad_id,'currency')}}/BTC </span><br/>
					<span style="color:#fff;font-size:25px;">
						@lang('crypto.trade_amount'): <b>{{$model->amount}} {{adinfo($model->ad_id,"currency")}} </b>
						({{$model->crypto_amount}} BTC)</span>
				</div>
			</div>
		</div>
	</div>
  	 <!--header section -->
@php

	$minutes = $model->timeout-time();
	$minutes = $minutes / 60;
	$minutes = ceil($minutes);
	if($minutes < 0) { $minutes = 0; }

	if($model->type == "sell_bitcoin") {
		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_1').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} elseif($model->type == "buy_bitcoin") {

		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_2').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} elseif($model->type == "sell_litecoin") {
		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_1_1').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} elseif($model->type == "buy_litecoin") {
		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_2_1').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} elseif($model->type == "sell_dogecoin") {
		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_1_2').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} elseif($model->type == "buy_dogecoin") {
		if($model->status == "0") {
			$status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
		} elseif($model->status == "1") {
			$status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
		} elseif($model->status == "2") {
			$status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
		} elseif($model->status == "3") {
			$status = '<span class="text text-info">'.__('crypto.status_3_2_2').'</span>';
		} elseif($model->status == "4") {
			$status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
		} elseif($model->status == "5") {
			$status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
		} elseif($model->status == "6") {
			$status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
		} elseif($model->status == "7") {
			$status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
		} else {
			$status = '<span class="text text-default">Unknown</span>';
		}
	} else { }



@endphp
	<!--header section -->
	 <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
							<div class="col-lg-12">
								@include('flash-message')
								@include('tradings.trading-notification')
							</div>
							<div class="col-md-8">
								<div class="panel panel-default">
									<div class="panel-body">
										<b>@lang('crypto.status'): </b> <span id="trade_status_{{$model->id}}">{!! $status !!}</span>
									</div>
								</div>

								@if($model->uid == $loginUser->id)
									@include('tradings.traders-trading')
								@elseif($model->trader == $loginUser->id)
									@include('tradings.clients-trading')
								@endif

							</div>
							@include('tradings.chat-section')
						</div>

					</div>
				</div>
			</div>
		</div>


@endsection