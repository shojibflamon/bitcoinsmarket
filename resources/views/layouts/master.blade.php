<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.partials.head')

    <body class="joblist style2 style3 title-image">

        @include('layouts.partials.nav-menu')

        @yield('content')

        @include('layouts.partials.footer')

    </body>

</html>


