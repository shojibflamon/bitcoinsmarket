<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title}}</title>

    <meta name="description" content="Description" />
	<meta name="keywords" content="keywords>" />
	<meta name="author" content="www.exchangesoftware.info">
    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}">

    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!--Custom template CSS-->
     <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
     <!--Custom template CSS Responsive-->
     <link href="{{ asset('assets/webcss/site-responsive.css') }}" rel="stylesheet">
       <!--Animated CSS -->
     <link href="{{ asset('assets/webcss/animate.css') }}" rel="stylesheet">
     <!--Owsome Fonts -->
     <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/css/cryptocoins.css') }}">

     <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/owlslider/owl-carousel/owl.carousel.css') }}">

    <!-- Default template -->
    <link rel="stylesheet" href="{{ asset('assets/owlslider/owl-carousel/owl.template.css') }}">

	<script type="text/javascript" src="{{ asset('assets/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/jquery.playSound.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/functions.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/uploader.min.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>