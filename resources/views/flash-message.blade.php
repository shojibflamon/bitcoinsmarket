@if(count($errors))
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
@endif
@if(session('flash-message'))
     <div class="alert alert-{{ session('alert_class') }}">
         @if(session('alert_class') == 'danger')
             <i class="fa fa-times"></i>
         @else
             <i class="fa fa-check"></i>
         @endif
           {{ session('flash-message') }}
    </div>
@endif