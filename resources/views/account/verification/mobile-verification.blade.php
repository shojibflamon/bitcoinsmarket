@if(!empty($addNumber))
    <div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.success_18')</div>
@endif
@if(!empty($verifyCode))
    <div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.success_17')</div>
@endif
@if(!empty($codeSendToSms))
    <div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.we_send_code') +{{$number}} @lang('crypto.please_enter_code')</div>
@endif
<h4>@lang('crypto.mobile_verification')</h4>
<hr/>
@if(getUserInfo($loginUser->id)->mobile_verified)
    <p><span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.mobile_verified')</span></p>
@else
    @if(getUserInfo($loginUser->id)->mobile_number)
        <p>@lang('crypto.click_sms_send') <b>{{getUserInfo($loginUser->id)->mobile_number}}</b></p>
        <form action="{{route('account.doVerification')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label>@lang('crypto.enter_sms_code')</label>
                <input type="text" class="form-control" name="sms_code">
                <input type="hidden" class="form-control" name="verify" value="send_sms_code">

            </div>
            <button type="submit" class="btn btn-primary btn-sm" name="btc_send_sms_code"><i class="fa fa-reply"></i> @lang('crypto.btn_send_sms_code')</button>
            <button type="submit" class="btn btn-primary btn-sm" name="btc_verify_sms_code"><i class="fa fa-check"></i> @lang('crypto.btn_verify_sms_code')</button>
        </form>
    @else
        <form action="{{route('account.doVerification')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label>@lang('crypto.your_mobile_number')</label>
                <input type="text" class="form-control" name="mobile_number" required>
                <input type="hidden" class="form-control" name="verify" value="add_number">
            </div>
            <button type="submit" class="btn btn-primary btn-sm" name="btc_add_number"><i class="fa fa-plus"></i> @lang('crypto.btn_add_number')</button>
        </form>
    @endif
@endif
<br>