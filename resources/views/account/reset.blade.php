@extends('layouts.master')
@section('content')

   <div class="container-fluid login_register header_area deximJobs_tabs">
    	<div class="row">
            <div class="container main-container">
                    <div class="col-lg-offset-1 col-lg-11 col-md-12 col-sm-12 col-xs-12">
                        <ul class="nav nav-pills ">
                            <li class="active"><a href="">@lang('crypto.forgot_password')</a></li>
                        </ul>

                    <div class="tab-content">
                        <div id="lost-password" class="tab-pane fade in active white-text">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 zero-padding-left">
                            	<p>
									@include('flash-message')
								</p>

								<form action="" method="POST" class="contact_us">
									{{csrf_field()}}
									<div class="form-group">
										<label>Email address</label>
										<input type="text" name="email">
									</div>
									<div class="form-group submit">
										<label>Submit</label>
										<input type="submit" name="btc_reset" value="@lang('crypto.btn_reset')" class="signin" id="signin">
									</div>
                        		</form>
                        	</div>
                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12  pull-right sidebar">
                            	<div class="widget">
                                	<h3>@lang('crypto.no_have_account')</h3>
                                    <ul>
                                    	<li>
                                        <p>@lang('crypto.register_info')</p></li>
										<li>
                                        <a href="{{route('register')}}" class="label job-type register">@lang('crypto.register')</a>

                                        </li>
                                    </ul>

                           		</div>
                            </div>
                        </div>

                    </div>


                    </div>

			</div>
       </div>
    </div>

@endsection