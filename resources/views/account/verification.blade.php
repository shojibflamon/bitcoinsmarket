@extends('layouts.master')
@section('content')
 <!-- Page Title-->
    	<div class="container-fluid blue-banner page-title bg-image">

        </div>
    <!-- Page Title-->
	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				@include('account.accordion-menu')
			</div>
			<div class="col-md-9">

				<div class="panel panel-default">
					<div class="panel-body">
						@include('flash-message')
						@php
							$status = getVerifyType();
						@endphp
						
						@if($status == '1')
							@include('account.verification.email-verification')
							@include('account.verification.doc-verification')
							@include('account.verification.mobile-verification')
						@elseif($status == '2')
							@include('account.verification.email-verification')
							@include('account.verification.doc-verification')
						@elseif($status == '3')
							@include('account.verification.doc-verification')
							@include('account.verification.mobile-verification')
						@elseif($status == '4')
							@include('account.verification.email-verification')
							@include('account.verification.mobile-verification')
						@elseif($status == '5')
							@include('account.verification.email-verification')
							@include('account.verification.doc-verification')
						@elseif($status == '6')
							@include('account.verification.doc-verification')
						@elseif($status == '7')
							@include('account.verification.email-verification')
						@elseif($status == '8')
							@include('account.verification.mobile-verification')
						@elseif($status == '9')
							{{verifiedUser()}}
						@endif

					</div>
				</div>
			</div>
		</div>
	</div>


@endsection