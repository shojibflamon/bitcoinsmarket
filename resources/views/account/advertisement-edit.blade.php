@extends('layouts.master')
@section('content')
<!-- Page Title-->
	<div class="container-fluid blue-banner page-title bg-image">
	</div>
<!-- Page Title-->

	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				@include('account.accordion-menu')
			</div>
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">

						@php
							if($model->type == "buy_bitcoin") {
								$type = 'Buy Bitcoin';
								$ext = 'BTC';
								$current_price = get_current_bitcoin_price();
								$crypto_price = convertBTCprice($model->price,$model->currency);
							} elseif($model->type == "sell_bitcoin") {
								$type = 'Sell Bitcoin';
								$ext = 'BTC';
								$current_price = get_current_bitcoin_price();
								$crypto_price = convertBTCprice($model->price,$model->currency);
							} elseif($model->type == "buy_litecoin") {
								$type = 'Buy Litecoin';
								$ext = 'LTC';
								$current_price = get_current_litecoin_price();
								$crypto_price = convertLTCprice($model->price,$model->currency);
							} elseif($model->type == "sell_litecoin") {
								$type = 'Sell Litecoin';
								$ext = 'LTC';
								$current_price = get_current_litecoin_price();
								$crypto_price = convertLTCprice($model->price,$model->currency);
							} elseif($model->type == "buy_dogecoin") {
								$type = 'Buy Dogecoin';
								$ext = 'DOGE';
								$current_price = get_current_dogecoin_price();
								$crypto_price = convertDOGEprice($model->price,$model->currency);
							} elseif($model->type == "sell_dogecoin") {
								$type = 'Sell Dogecoin';
								$ext = 'DOGE';
								$current_price = get_current_dogecoin_price();
								$crypto_price = convertDOGEprice($model->price,$model->currency);
							} else { }
						@endphp
						
						<h4>@lang('crypto.menu_advertisements') <small>@lang('crypto.edit')</small></h4>
						<hr/>
						@include('flash-message')
						@include('account.edit-advertise-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection