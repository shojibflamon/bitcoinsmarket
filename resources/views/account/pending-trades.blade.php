@if(count($trades) > 0)
    @foreach($trades as $single)
        @php

        if($single->type == "sell_bitcoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_1').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } elseif($single->type == "buy_bitcoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_2').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } elseif($single->type == "sell_litecoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_1_1').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } elseif($single->type == "buy_litecoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_2_1').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } elseif($single->type == "sell_dogecoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_1').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_1').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_1_2').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } elseif($single->type == "buy_dogecoin") {
            if($single->status == "0") {
                $status =  '<span class="text text-info">'.__('crypto.status_0').'</span>';
            } elseif($single->status == "1") {
                $status =  '<span class="text text-info">'.__('crypto.status_1_2').'</span>';
            } elseif($single->status == "2") {
                $status = '<span class="text text-info">'.__('crypto.status_2_2').'</span>';
            } elseif($single->status == "3") {
                $status = '<span class="text text-info">'.__('crypto.status_3_2_2').'</span>';
            } elseif($single->status == "4") {
                $status = '<span class="text text-danger">'.__('crypto.status_4').'</span>';
            } elseif($single->status == "5") {
                $status = '<span class="text text-danger">'.__('crypto.status_5').'</span>';
            } elseif($single->status == "6") {
                $status = '<span class="text text-danger">'.__('crypto.status_6').'</span>';
            } elseif($single->status == "7") {
                $status = '<span class="text text-success">'.__('crypto.status_7').'</span>';
            } else {
                $status = '<span class="text text-default">Unknown</span>';
            }
        } else { }


        if($single->uid == $loginUser->id) { $youclient = '('.__('crypto.you').')'; } else { $youclient = ''; }
        if($single->trader == $loginUser->id) { $youtrader = '('.__('crypto.you').')'; } else { $youtrader = ''; }
        if($single->type == "sell_bitcoin") { $type = __('crypto.sell'); }
        elseif($single->type == "buy_bitcoin") { $type = __('crypto.buy'); }
        elseif($single->type == "sell_litecoin") { $type = __('crypto.sell'); }
        elseif($single->type == "buy_litecoin") { $type = __('crypto.buy'); }
        elseif($single->type == "sell_dogecoin") { $type = __('crypto.sell'); }
        elseif($single->type == "buy_dogecoin") { $type = __('crypto.buy'); }
        else { $type = @lang('crypto.buy'); }
        if($single->network == "Bitcoin") { $ext = 'BTC'; }
        elseif($single->network == "Litecoin") { $ext = 'LTC'; }
        elseif($single->network == "Dogecoin") { $ext = 'DOGE'; }
        else { $ext = 'BTC'; }

        @endphp

        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>@lang('crypto.type')</th>
                            <th>@lang('crypto.client')</th>
                            <th>@lang('crypto.trader')</th>
                            <th>@lang('crypto.amount')</th>
                            <th>@lang('crypto.payment_method')</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{{$type}}</td>
                            <td><a href="{{route('userProfile',getUserInfo($single->uid)->username)}}">{{getUserInfo($single->uid)->username}}</a> {{$youclient}}</td>
                            <td><a href="{{route('userProfile',getUserInfo($single->trader)->username)}}">{{getUserInfo($single->trader)->username}} </a> {{$youtrader}}</td>
                            <td>{{$single->amount}} {{adinfo($single->ad_id,'currency')}} ({{$single->crypto_amount}} {{$ext}})</td>
                            <td>{{adinfo($single->ad_id,'payment_method')}}</td>
                        </tr>
                    </tbody>

                    <thead>
                        <tr>
                            <th colspan="5">
                                @lang('crypto.status'): {!! $status !!}
                            </th>
                        </tr>
                    </thead>
                </table>

                <a href="{{route('account.singleTrade',$single->id)}}" class="btn btn-primary">@lang('crypto.btn_process_trade')</a>
                <a href="{{route('account.cancelTrade',$single->id)}}" class="btn btn-danger">@lang('crypto.btn_cancel_trade')</a>
            </div>
        </div>

    @endforeach
@else
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.info_8')</div>
@endif
