@extends('layouts.master')
@section('content')

    <div class="container-fluid login_register header_area deximJobs_tabs">
    	<div class="row">
            <div class="container main-container">
                <div class="col-lg-offset-1 col-lg-11 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-pills ">
                        <li><a href="{!! route('register') !!}">@lang('crypto.register')</a></li>
                        <li class="active"><a href="{!! route('login') !!}">@lang('crypto.login')</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="login" class="tab-pane fade in active white-text">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 zero-padding-left">
                                @include('flash-message')
                                @include('login-form')
                            </div>
                                
                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12  pull-right sidebar">
                                <div class="widget">
                                    <h3>@lang('crypto.no_have_account')</h3>
                                    <ul>
                                        <li>
                                            <p>@lang('crypto.register_info')</p>
                                        </li>
                                        <li>
                                            <a href="{!! route('register') !!}" class="label job-type register">@lang('crypto.register')</a>
                                        </li>
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
			</div>
       </div>
    </div> 

@endsection