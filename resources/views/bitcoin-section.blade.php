<div class="container main-container list-style3">
    <div class="row">
        <div class="col-lg-12">
            <div class="tab_filters">
                <div class="col-lg-4">
                    <h5>{{$coinTitle}}</h5>
                </div>

                @php
                    $typeArray = explode('_',$type);
                    $allPmRoute = $typeArray[0].ucfirst($typeArray[1]);
                @endphp

                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 filters pull-right filter-category">
                    <ul class="nav nav-pills">

                        <li class="web-designer"><a href="{{route('filterByPaymentMethod',[$type,'PayPal'])}}">PayPal</a></li>
                        <li class="fianance"><a href="{{route('filterByPaymentMethod',[$type,'Perfect Money'])}}">Perfect Money</a></li>
                        <li class="education"><a href="{{route('filterByPaymentMethod',[$type,'Payeer'])}}">Payeer</a></li>
                        <li class="food-service"><a href="{{route('filterByPaymentMethod',[$type,'Skrill'])}}">Skrill</a></li>
                        <li class="health-services"><a href="{{route('filterByPaymentMethod',[$type,'Neteller'])}}">Neteller</a></li>
                        <li class="automative"><a href="{{route('filterByPaymentMethod',[$type,'AdvCash'])}}">AdvCash</a></li>
                        <li class="automative"><a href="{{route('filterByPaymentMethod',[$type,'Bank Transfer'])}}">Bank Transfer</a></li>
                        <li class="all"><a href="{{route($allPmRoute)}}">All</a></li>
                    </ul>
                </div>
            </div>
            <div class="jobs-result">
                <!--Search Result 01-->
                <div class="jobs list-style2">
                    @if(count($postAdds) > 0)
                        @foreach($postAdds as $single)

                            <div class="filter-result 01">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="desig">
                                        <span class="pull-left"><a href="{{route('userProfile',$single->user->username)}}" id="user_status" data-toggle="tooltip" data-placement="top" title="{{activity_time($single->User->id)}}"><h3>{{$single->User->username}}</h3></a>
                                            <small>{{activity_time($single->User->id)}}</small>
                                        </span>
                                        <span class="pull-right">
                                            @php
                                                $type = explode('_',$single->type);
                                                if($type[0] == 'buy'){
                                                    $btn = __('crypto.btn_sell');
                                                }else{
                                                    $btn = __('crypto.btn_buy');
                                                }
                                            @endphp
                                            <a href="{{route('postAdView',$single->id)}}"><span class="label job-type job-contract ">{{$btn}}</span></a>
                                        </span>
                                    </div>

                                    <div class="job-footer">
                                        <ul>
                                            <li>@lang('crypto.price'):
                                                @if($single->network == "Bitcoin")
                                                    {{convertBTCprice($single->price,$single->currency)}} {{$single->currency}}/BTC
                                                @elseif($single->network == "Litecoin")
                                                    {{convertLTCprice($single->price,$single->currency)}} {{$single->currency}}/LTC
                                                @elseif($single->network == "Dogecoin")
                                                    {{convertDOGEprice($single->price,$single->currency)}}  {{$single->currency}}/DOGE
                                                @else
                                                    asdf
                                                @endif
                                            </li>
                                            <li>@lang('crypto.limits'): {{$single->min_amount}} - {{$single->max_amount}} {{$single->currency}}</li>
                                            <li>{{$single->payment_method}}</a>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @else
                        <div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.no_ad_for_display')</div>
                    @endif

                    <!--jobs result-->

                </div>
                     <!--jobs result-->
                <div class="clearfix"></div>

                <?php if(false) { ?>
                    <div class="col-lg-12">
                        <a href="" class="btn btn-default" id="load_more" style="max-width:250px;"> @lang('crypto.show_more_ads') <span class="glyphicon glyphicon-menu-down"></span></a>
                    </div>
                <?php } ?>
            </div>
                 <!--Search Result 01-->
        </div>
    </div>
</div>